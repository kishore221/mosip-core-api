package io.mosip.core.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jayway.jsonpath.ReadContext;

import io.mosip.core.api.dao.ResidentBiometricsRepo;
import io.mosip.core.api.dao.ResidentRepo;
import io.mosip.core.api.dao.VINRepo;
import io.mosip.core.api.model.Resident;
import io.mosip.core.api.model.ResidentBiometrics;
import io.mosip.core.api.response.AddressResponse;
import io.mosip.core.api.response.BiometricsResponse;
import io.mosip.core.api.response.UINReprintResponse;
import io.mosip.core.api.response.VINGeneration;
import io.mosip.core.api.response.VINRevoke;
import io.mosip.core.api.service.NotificationService;

@RestController
@RequestMapping("/mosip")
public class APIController {
	
	@Autowired
	VINRepo vinrepo;
	
	@Autowired
	ResidentRepo residentrepo;
	
	@Autowired
	NotificationService notificationservice;
	
	@Autowired
	ResidentBiometricsRepo residentbiometricsrepo;
	
	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	@RequestMapping("/test")
	public String test() {
		return "Working";
	}
	
	
	@RequestMapping(value="/vin/generate" ,method=RequestMethod.POST)
	@ResponseBody
	public VINGeneration genVINPOST(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return new VINGeneration(uin , 0L,false,"Invalid UIN",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
		}else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {
				VINGeneration vingen = vinrepo.findById(uin).orElse(null);
				if(vingen != null) {
					if(vingen.getRevoke_status().equals("revoked"))
						return new VINGeneration(uin , 0L,false,"VIN have been revoked",sdf.format(new Date(System.currentTimeMillis())),"revoked");
					else {
						String email = "kishorekunal221@gmail.com";
						notificationservice.sendNotification(vingen.getVin(),email);
						return vingen;
					}
				}
				else {
					long vin=System.currentTimeMillis();
					vingen = new VINGeneration(uin , vin,true,"Accepted",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
					vinrepo.save(vingen);
					//String email=res.getEmail();
					String email = "kishorekunal221@gmail.com";
					notificationservice.sendNotification(vin,email);
					return vingen;
				}
							
			}
			else {
				return new VINGeneration(uin , 0L,false,"No UIN Exists",sdf.format(new Date(System.currentTimeMillis())),"NA");
			}
		
		}
	}
	

	@RequestMapping(value="/vin/generate" ,method=RequestMethod.GET)
	@ResponseBody
	public VINGeneration genVINGET(@RequestBody VINGeneration vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return new VINGeneration(uin , 0L,false,"Invalid UIN",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
			
		}else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {
				VINGeneration vingen = new VINGeneration(uin , System.currentTimeMillis(),true,"Accepted",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
				vinrepo.save(vingen);
				return vingen;			
			}
			else {
				return new VINGeneration(uin , 0L,false,"No UIN Exists of this type",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
			}
			
		}
		
	}
	
	@RequestMapping(value="/vin/generate/do" ,method=RequestMethod.GET)
	public ResponseEntity<VINGeneration> genVINGET(@RequestParam(value="uin", defaultValue="NA") String uin,
			@RequestParam(value="sentOTP", defaultValue="NA") String sentOTP,@RequestParam(value="enterOTP", defaultValue="NA") String enterOTP) {
		if(uin==null||uin.equalsIgnoreCase("NA")||sentOTP==null||sentOTP.equalsIgnoreCase("NA")||enterOTP==null||enterOTP.equalsIgnoreCase("NA")) {
			return ResponseEntity.ok()
					.header("Access-Control-Allow-Origin", "*")
			        .body(new VINGeneration(uin , 0L,false,"Invalid INPUT",sdf.format(new Date(System.currentTimeMillis())),"not_revoked"));
		}else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {
				if(sentOTP.equals(enterOTP)) {
				long vin=System.currentTimeMillis();
				VINGeneration vingen = new VINGeneration(uin , vin,true,"Accepted",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
				vinrepo.save(vingen);
				//String email=res.getEmail();
				String email = "kishorekunal221@gmail.com";
				notificationservice.sendNotification(vin,email);
				 return ResponseEntity.ok()
					        .header("Access-Control-Allow-Origin", "*")
					        .body(vingen);
				//return vingen;
			}else {
				//otp mismatch
				VINGeneration vingen = new VINGeneration(uin , 0L,false,"OTP Validation Failed",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
				 return ResponseEntity.ok()
					        .header("Access-Control-Allow-Origin", "*")
					        .body(vingen);
				
			}
			}
			else {
				return ResponseEntity.ok()
						.header("Access-Control-Allow-Origin", "*")
				        .body(new VINGeneration(uin , 0L,false,"No UIN Exists",sdf.format(new Date(System.currentTimeMillis())),"not_revoked"));
			}
		
		}
		
	}
	
	@RequestMapping(value="/vin/generate/sendOTP" ,method=RequestMethod.GET)
	public ResponseEntity<VINGeneration> sendOTP(@RequestParam(value="uin", defaultValue="NA") String uin) {
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return ResponseEntity.ok()
					.header("Access-Control-Allow-Origin", "*")
			        .body(new VINGeneration(uin , 0L,false,"Invalid UIN OTP Failed",sdf.format(new Date(System.currentTimeMillis())),"not_revoked"));
		}else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {
				long vin=System.currentTimeMillis();
				VINGeneration vingen = new VINGeneration(uin ,2020L,true,"OTP SENT",sdf.format(new Date(System.currentTimeMillis())),"not_revoked");
				//vinrepo.save(vingen);
				//String email=res.getEmail();
				String email = "kishorekunal221@gmail.com";
				notificationservice.sendOTP(vin, email, "2020");
				 return ResponseEntity.ok()
					        .header("Access-Control-Allow-Origin", "*")
					        .body(vingen);
				//return vingen;			
			}
			else {
				return ResponseEntity.ok()
						.header("Access-Control-Allow-Origin", "*")
				        .body(new VINGeneration(uin , 0L,false,"No UIN Exists",sdf.format(new Date(System.currentTimeMillis())),"not_revoked"));
			}
		
		}
		
	}
	
	@RequestMapping(value="/vin/revoke" ,method=RequestMethod.POST)
	@ResponseBody
	public VINRevoke revVINPOST(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			
			return new VINRevoke(false,"Invalid UIN",sdf.format(new Date(System.currentTimeMillis())));
		}else {
			VINGeneration vingen = vinrepo.findById(uin).orElse(null);
			if(vingen != null) {
				//vinrepo.deleteById(uin);
				vingen.setRevoke_status("revoked");
				vinrepo.save(vingen);
			    return new VINRevoke(true,"UIN Revoked Successfully",sdf.format(new Date(System.currentTimeMillis())));
			}
			else {
			    return new VINRevoke(false,"No UIN exists of this type",sdf.format(new Date(System.currentTimeMillis())));

			}
		}
	}
	

	@RequestMapping(value="/vin/revoke" ,method=RequestMethod.GET)
	@ResponseBody
	public VINRevoke revVINGET(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return new VINRevoke(false,"Invalid uin",sdf.format(new Date(System.currentTimeMillis())));
		}else {
			VINGeneration vingen = vinrepo.findById(uin).orElse(null);
			if(vingen != null) {
				//vinrepo.deleteById(uin);
				vingen.setRevoke_status("revoked");
				vinrepo.save(vingen);
			    return new VINRevoke(true,"UIN Revoked Successfully",sdf.format(new Date(System.currentTimeMillis())));
			}
			else {
			    return new VINRevoke(false,"No UIN exists of this type",sdf.format(new Date(System.currentTimeMillis())));

			}
			
		}
	}
	
	@RequestMapping(value="/uin/lockBiometrics" ,method=RequestMethod.POST)
	@ResponseBody
	//public ResponseEntity<BiometricsResponse> lockBioGET(@RequestParam(value="uin", defaultValue="NA") String uin,
	//		@RequestParam(value="sentOTP", defaultValue="NA") String sentOTP,@RequestParam(value="enterOTP", defaultValue="NA") String enterOTP) {
	public BiometricsResponse lockBioGET(@RequestBody JSONObject vinG){
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['uin']");
		String sentOTP = ctx.read("$['request']['sentOTP']");
		String enterOTP = ctx.read("$['request']['enterOTP']");
		if(uin==null||uin.equalsIgnoreCase("NA")||sentOTP==null||sentOTP.equalsIgnoreCase("NA")||enterOTP==null||enterOTP.equalsIgnoreCase("NA")) {
			return new BiometricsResponse(false,"Invalid Input","No Status" ,sdf.format(new Date(System.currentTimeMillis())));
			//return ResponseEntity.ok()
			//		.header("Access-Control-Allow-Origin", "*")
			//       .body(new BiometricsResponse(false,"Invalid Input","No Status" ,sdf.format(new Date(System.currentTimeMillis()))));
		}else {
			ResidentBiometrics res = residentbiometricsrepo.findById(uin).orElse(null);
			if(res != null){
				if(sentOTP.equals(enterOTP)) {
					if(res.getBiometrics_status().equals("unlocked")) {	
						res.setBiometrics_status("locked");
						residentbiometricsrepo.save(res);
						BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"Biometrics Locked", sdf.format(new Date(System.currentTimeMillis())));			
						
						return bioresponse;
						//return ResponseEntity.ok()
					    //    .header("Access-Control-Allow-Origin", "*")
					    //    .body(bioresponse);
					}
					else {
						BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"Biometrics Already Locked", sdf.format(new Date(System.currentTimeMillis())));			
						
						return bioresponse;
						//return ResponseEntity.ok()
					    //    .header("Access-Control-Allow-Origin", "*")
					    //    .body(bioresponse);
					}
				//return vingen;
			 }
			else {
			//otp mismatch
			 BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"OTP Validation Failed", sdf.format(new Date(System.currentTimeMillis())));			
			 return bioresponse;
			 //return ResponseEntity.ok()
			//	        .header("Access-Control-Allow-Origin", "*")
			//	        .body(bioresponse);
			
			}
		}
		else {
			return new BiometricsResponse(true, uin ,"No UIN Exists", sdf.format(new Date(System.currentTimeMillis())));
			//return ResponseEntity.ok()
			//		.header("Access-Control-Allow-Origin", "*")
			//        .body(new BiometricsResponse(true, uin ,"No UIN Exists", sdf.format(new Date(System.currentTimeMillis()))));	
		}
		
		}
		
		
	}
	
	/*------  Unlock Biometrics for UIN   -------*/
	
	@RequestMapping(value="/uin/unlockBiometrics" ,method=RequestMethod.POST)
	//public BiometricsResponse unlockBioGET(@RequestParam(value="uin", defaultValue="NA") String uin,
	//		@RequestParam(value="sentOTP", defaultValue="NA") String sentOTP,@RequestParam(value="enterOTP", defaultValue="NA") String enterOTP) {
	public BiometricsResponse unlockBioGET(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['uin']");
		String sentOTP = ctx.read("$['request']['sentOTP']");
		String enterOTP = ctx.read("$['request']['enterOTP']");
		if(uin==null||uin.equalsIgnoreCase("NA")||sentOTP==null||sentOTP.equalsIgnoreCase("NA")||enterOTP==null||enterOTP.equalsIgnoreCase("NA")) {
			return new BiometricsResponse(false,"Invalid Input","No Status" ,sdf.format(new Date(System.currentTimeMillis())));
			/*return ResponseEntity.ok()
					.header("Access-Control-Allow-Origin", "*")
			        .body(new BiometricsResponse(false,"Invalid Input","No Status" ,sdf.format(new Date(System.currentTimeMillis()))));*/
		}else {
			ResidentBiometrics res = residentbiometricsrepo.findById(uin).orElse(null);
			if(res != null){
				if(sentOTP.equals(enterOTP)) {
					if(res.getBiometrics_status().equals("locked")) {	
						res.setBiometrics_status("unlocked");
						residentbiometricsrepo.save(res);
						BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"Biometrics Unocked", sdf.format(new Date(System.currentTimeMillis())));			
						
						return bioresponse;
						/*return ResponseEntity.ok()
					        .header("Access-Control-Allow-Origin", "*")
					        .body(bioresponse);*/
					}
					else {
						BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"Biometrics Already Unlocked", sdf.format(new Date(System.currentTimeMillis())));			
						
						return bioresponse;
						/*return ResponseEntity.ok()
					        .header("Access-Control-Allow-Origin", "*")
					        .body(bioresponse);*/
					}
				//return vingen;
			 }
			else {
			//otp mismatch
			 BiometricsResponse bioresponse = new BiometricsResponse(true, uin ,"OTP Validation Failed", sdf.format(new Date(System.currentTimeMillis())));			
			 return bioresponse;
			 /*return ResponseEntity.ok()
				        .header("Access-Control-Allow-Origin", "*")
				        .body(bioresponse);*/
			
			}
		}
		else {
			return new BiometricsResponse(true, uin ,"No UIN Exists", sdf.format(new Date(System.currentTimeMillis())));
			/*return ResponseEntity.ok()
					.header("Access-Control-Allow-Origin", "*")
			        .body(new BiometricsResponse(true, uin ,"No UIN Exists", sdf.format(new Date(System.currentTimeMillis()))));*/
		}
		
		}
		
		
	}
	
	
	@RequestMapping(value="/uin/reprint" ,method=RequestMethod.POST)
	@ResponseBody
	public UINReprintResponse requestReprintPOST(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return new UINReprintResponse(null,false,"Invalid UIN",sdf.format(new Date(System.currentTimeMillis())));
		}
		else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {
				return new UINReprintResponse(res,true,"Reprint request taken Successfully",sdf.format(new Date(System.currentTimeMillis())));			
			}
			else {
				return new UINReprintResponse(res,false,"UIN Not Exists of this type",sdf.format(new Date(System.currentTimeMillis())));
			}
			
		}
	}
	

	@RequestMapping(value="/uin/reprint" ,method=RequestMethod.GET)
	@ResponseBody
	public UINReprintResponse requestReprintGET(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['identity']['uin']");
		if(uin==null||uin.equalsIgnoreCase("NA")) {
			return new UINReprintResponse(null,false,"Invalid UIN",sdf.format(new Date(System.currentTimeMillis())));
		}
		else {
			Resident res = residentrepo.findById(uin).orElse(null);
			if(res != null) {	
				return new UINReprintResponse(res,true,"Reprint request taken Successfully",sdf.format(new Date(System.currentTimeMillis())));			
			}
			else {
				return new UINReprintResponse(res,false,"UIN Not Exists of this type",sdf.format(new Date(System.currentTimeMillis())));
			}
			
		}
	}
	
	@RequestMapping(value="/demographic/update" ,method=RequestMethod.POST)
	@ResponseBody
	public AddressResponse updateRequest(@RequestBody JSONObject vinG) {
		ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(vinG);
		String uin = ctx.read("$['request']['demographicDetails']['uin']");
		String name = ctx.read("$['request']['demographicDetails']['name']");
		String father_name = ctx.read("$['request']['demographicDetails']['father_name']");
		String dob = ctx.read("$['request']['demographicDetails']['dob']");
		String house_no = ctx.read("$['request']['demographicDetails']['house_no']");
		String street = ctx.read("$['request']['demographicDetails']['street']");
		String city = ctx.read("$['request']['demographicDetails']['city']");
		String dist = ctx.read("$['request']['demographicDetails']['dist']");
		String state = ctx.read("$['request']['demographicDetails']['state']");
		String country = ctx.read("$['request']['demographicDetails']['country']");
		Resident resident = new Resident( uin,  name,  father_name,  dob,  house_no,  street, city,  dist,  state,  country);
		
		
		//Resident resident = ctx.read("$['request']['demographicDetails']");
		AddressResponse resp=validateData(resident);
		if(resp.getStatus()==false)
			return resp;
		else {
			residentrepo.save(resident);
			return resp;
		}
		
		//System.out.println("working");
		//return "updating demographic";
	}
	//
	public AddressResponse validateData(Resident resident) {
		AddressResponse resp=new AddressResponse();
		resp.setUin(resident.getUin());
		resp.setTimestamp(sdf.format(new Date(System.currentTimeMillis())));
		if(resident.getUin()==null||resident.getCity().length()<=0) {
			resp.setMessage("Invalid UIN");
			resp.setStatus(false);
		}else if(resident.getCity()==null||resident.getCity().length()<=0) {
			resp.setMessage("Invalid City");
			resp.setStatus(false);
		}else if(resident.getDist()==null||resident.getDist().length()<=0) {
			resp.setMessage("Invalid District");
			resp.setStatus(false);
		}else if(resident.getStreet()== null ||resident.getStreet().length()<=0) {
			resp.setMessage("Invalid Street");
			resp.setStatus(false);
		}else if(resident.getState()== null ||resident.getState().length()<=0) {
			resp.setMessage("Invalid State");
			resp.setStatus(false);
		}else if(resident.getHouse_no()== null ||resident.getHouse_no().length()<=0) {
			resp.setMessage("Invalid House No.");
			resp.setStatus(false);
		}else {
			resp.setMessage("Update Request Recieved");
			resp.setStatus(true);
		}
		return resp;
	}

}
