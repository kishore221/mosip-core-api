package io.mosip.core.api.response;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VINGeneration {
	@Id
	private String uin;
	private long vin;
	private boolean status;
	private String message;
	private String timestamp;
	private String revoke_status;
	
	public VINGeneration() {
		
	}
	
	
	public VINGeneration(String uin , long vin, boolean status, String message, String timestamp, String revoke_status) {
		super();
		this.uin = uin;
		this.vin = vin;
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
		this.revoke_status=revoke_status;
	}

	public String getUin() {
		return uin;
	}


	public void setUin(String uin) {
		this.uin = uin;
	}


	public long getVin() {
		return vin;
	}

	public void setVin(long vin) {
		this.vin = vin;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getRevoke_status() {
		return revoke_status;
	}


	public void setRevoke_status(String revoke_status) {
		this.revoke_status = revoke_status;
	}


	@Override
	public String toString() {
		return "VINGeneration [uin=" + uin + ", vin=" + vin + ", status=" + status + ", message=" + message
				+ ", timestamp=" + timestamp + ", revoke_status=" + revoke_status +"]";
	}

	
	
	
	
}
