package io.mosip.core.api.response;

public class VINRevoke {
	private boolean status;
	private String message;
	private String timestamp;
	
	public VINRevoke() {
		
	}
	
	
	public VINRevoke(boolean status, String message, String timestamp) {
		super();
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "VINGeneration [status=" + status + ", message=" + message + ", timestamp=" + timestamp
				+ "]";
	}
	
	
	
}
