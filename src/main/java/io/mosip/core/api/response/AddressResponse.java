package io.mosip.core.api.response;

public class AddressResponse {
	
	
	private String uin;
	private boolean status;
	private String message;
	private String timestamp;
	
	
	public AddressResponse() {}
	
	

	@Override
	public String toString() {
		return "AddressResponse [uin=" + uin + ", status=" + status + ", message=" + message + ", timestamp="
				+ timestamp + "]";
	}



	public AddressResponse(String uin, boolean status, String message, String timestamp) {
		super();
		this.uin = uin;
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
	}



	public String getUin() {
		return uin;
	}


	public void setUin(String string) {
		this.uin = string;
	}


	public boolean getStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
